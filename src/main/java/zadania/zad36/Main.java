package zadania.zad36;

public class Main {

    private static Thread t1;
    private static Thread t2;

    public static void main(String[] args) {

        t1 = new Thread(new ThreadPlaygroundRunnable("Jola"));
        t2 = new Thread(new ThreadPlaygroundRunnable("Ola"));

        t1.start();
        t2.start();
    }
}
