package zadania.zad5;

import java.util.HashMap;
import java.util.Map;

public class SDAHashSet<E> {
    private Map<E, Object> map;

    private final static Object DUMP = new Object();

    public SDAHashSet() {
        this.map = new HashMap<>();
    }

    public boolean add(E element){
      return map.put(element, DUMP) !=null;
    }
    public void remove(E element){
        map.remove(element);
    }
    public int size(){
        return map.size();
    }
    public boolean contains(E element){
        return map.containsKey(element);
    }
    public void clear(){
        map.clear();
    }
}
