package zadania.zad5;

public class Main {

    public static void main(String[] args) {
        SDAHashSet<String> sdaHashSet = new SDAHashSet<>();

        sdaHashSet.add("Ola");
        System.out.println(sdaHashSet.size());
        sdaHashSet.clear();
        System.out.println(sdaHashSet.size());
        sdaHashSet.add("Ola");
        sdaHashSet.add("Ola");
        System.out.println(sdaHashSet.size());
        sdaHashSet.add("Andrzej");
        System.out.println(sdaHashSet.size());
        sdaHashSet.remove("Andrzej");
        System.out.println(sdaHashSet.size());

    }
}
