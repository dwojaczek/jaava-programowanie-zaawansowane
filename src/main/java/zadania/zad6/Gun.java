package zadania.zad6;

public class Gun {
    private int capacity;
    private int currentLoad;

    public Gun(int capacity) {
        this.capacity = capacity;
    }
    public void loadBullet(){
        if (capacity> currentLoad){
            currentLoad++;
            System.out.println("loaded, current load: "+currentLoad);
        }else {
            System.out.println("cannot load, magazine full");
        }
    }
    public boolean isLoaded(){
        return currentLoad>0;
    }
    public void shot(){
        if (isLoaded()){
            currentLoad--;
            System.out.println("bang");
        }
        else {
            System.out.println("empty :(");
        }
    }
}
