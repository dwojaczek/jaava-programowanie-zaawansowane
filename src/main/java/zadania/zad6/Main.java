package zadania.zad6;

public class Main {

    public static void main(String[] args) {
        Gun gun = new Gun(3);

        gun.loadBullet();
        gun.loadBullet();
        gun.loadBullet();

        gun.shot();
        gun.shot();
        gun.shot();
        gun.shot();

        gun.loadBullet();
        gun.loadBullet();
        gun.loadBullet();
        gun.loadBullet();

    }
}
