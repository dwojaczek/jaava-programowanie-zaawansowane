package zadania.zad39;

import java.util.Random;

public class Sensor implements Runnable {

    private ScoreBoard scoreBoard;

    public Sensor(ScoreBoard scoreBoard) {
        this.scoreBoard = scoreBoard;
    }

    @Override
    public void run() {
        Random random = new Random();

        while (true){
            int currentScore = scoreBoard.readScore();

            try {
                Thread.sleep(random.nextInt(10000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int updated = currentScore + random.nextInt(3);
            System.out.println("updated score: "+ updated);

            scoreBoard.updateScore(currentScore, updated);
        }


    }
}
