package zadania.zad39;

public class Monitor implements Runnable{
    private ScoreBoard scoreBoard;

    public Monitor(ScoreBoard scoreBoard) {
        this.scoreBoard = scoreBoard;
    }

    @Override
    public void run() {

        while (true){
            System.out.println("Current score: "+scoreBoard.readScore());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
