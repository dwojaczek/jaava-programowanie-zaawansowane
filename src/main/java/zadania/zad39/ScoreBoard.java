package zadania.zad39;

import java.util.concurrent.atomic.AtomicInteger;

public class ScoreBoard {

    AtomicInteger score = new AtomicInteger();

    public int readScore(){
        return score.get();
    }

    public void updateScore(int currentScore, int updatedScore){

//        if (currentScore == score.get()){
//            score.set(updatedScore);
//        }

        //to jest to samo co wyzej
        score.compareAndSet(currentScore, updatedScore);

    }
}
