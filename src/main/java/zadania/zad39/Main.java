package zadania.zad39;

public class Main {

    public static void main(String[] args) {
        ScoreBoard scoreBoard = new ScoreBoard();

        Sensor sensor1 = new Sensor(scoreBoard);
        Sensor sensor2 = new Sensor(scoreBoard);
        Sensor sensor3 = new Sensor(scoreBoard);
        Monitor monitor1 = new Monitor(scoreBoard);

        Thread t1 = new Thread(sensor1);
        Thread t2 = new Thread(monitor1);
        Thread t3 = new Thread(sensor2);
        Thread t4 = new Thread(sensor3);

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }
}
