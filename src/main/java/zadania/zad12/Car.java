package zadania.zad12;

import java.util.List;
import java.util.Objects;

public class Car {

    private String name;
    private String model;
    private String productionYear;
    private int price;
    private List<Manufacturer> manufacturers;
    private Engine engineType;

    public Car(String name, String model, String productionYear, List<Manufacturer> manufacturers, Engine engineType, int price) {
        this.name = name;
        this.model = model;
        this.productionYear = productionYear;
        this.manufacturers = manufacturers;
        this.engineType = engineType;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(String productionYear) {
        this.productionYear = productionYear;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public Engine getEngineType() {
        return engineType;
    }

    public void setEngineType(Engine engineType) {
        this.engineType = engineType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(name, car.name) && Objects.equals(model, car.model) && Objects.equals(productionYear, car.productionYear) && Objects.equals(manufacturers, car.manufacturers) && engineType == car.engineType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, productionYear, manufacturers, engineType);
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", productionYear='" + productionYear + '\'' +
                ", price=" + price +
                ", manufacturers=" + manufacturers +
                ", engineType=" + engineType +
                '}';
    }
}
