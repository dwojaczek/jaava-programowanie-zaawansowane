package zadania.zad13;

import zadania.zad12.Car;
import zadania.zad12.Engine;
import zadania.zad12.Manufacturer;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Manufacturer lambo = new Manufacturer("Lamborghini", 1920, "Italy");
        List<Manufacturer> manufacturers = List.of(lambo);
        Car car = new Car("Lambo", "Diablo", "2002",manufacturers, Engine.V12, 2000000 );

        CarService carService = new CarService();
        carService.addCar(car);

        List<Car> allProducedBy = carService.getAllProducedBy(lambo);
        System.out.println(allProducedBy);

    }
}
