package zadania.zad13;

import zadania.zad12.Car;
import zadania.zad12.Engine;
import zadania.zad12.Manufacturer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CarService {

    private List<Car> cars = new ArrayList<>();

    public void addCar(Car car){
        cars.add(car);
    }
    public void removeCar(Car car){
        cars.remove(car);
    }

    public List<Car> getAllCars(){
        return cars;
    }

    public List<Car> getAllWithV12Engine(){
      return cars.stream().filter(car->car.getEngineType() == Engine.V12).collect(Collectors.toList());
    }
    public List<Car> getAllProducedBefore1999(){
        return cars.stream().filter(car -> Integer.getInteger(car.getProductionYear())<1999).collect(Collectors.toList());
    }

    public Car getMostExpensive(){
        Collections.sort(cars, (new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getPrice() - o2.getPrice();
            }
        }));
        return cars.get(0);
    }
    public Car getTheCheapest(){
        Collections.sort(cars, (new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o2.getPrice() - o1.getPrice();
            }
        }));
        return cars.get(0);
    }
    public List<Car> getCarsWithAtLeast3Manufacturers(){
       return cars.stream().filter(c->c.getManufacturers().size() >=3).collect(Collectors.toList());

    }

    public List<Car> getAllProducedBy(Manufacturer manufacturer){
        return  cars.stream().filter(c ->c.getManufacturers().contains(manufacturer)).collect(Collectors.toList());
    }


}
