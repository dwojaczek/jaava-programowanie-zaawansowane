package zadania.zad4;

public class Main {



    public static void main(String[] args) {
        Storage storage = new Storage();

        storage.addToStorage("car", "audi");
        storage.addToStorage("car", "subaru");
        storage.addToStorage("car", "jaguar");

        storage.addToStorage("animal", "dog");
        storage.addToStorage("animal", "cat");
        storage.addToStorage("animal", "jaguar");

        storage.printValues("car");
        storage.printKeys("jaguar");
    }
}
