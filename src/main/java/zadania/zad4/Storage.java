package zadania.zad4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {

    private Map<String, List<String>> map;

    public Storage() {
        this.map = new HashMap<>();
    }

    public void addToStorage(String key, String value){
        if (map.containsKey(key)){
            List<String> values = map.get(key);
            values.add(value);
        }else {
            List<String> values = new ArrayList<>();
            values.add(value);
            map.put(key, values);
        }
    }
    public void printValues(String key){
        List<String> strings = map.get(key);

        System.out.println(strings);
    }

    public void printKeys(String value){
        map.keySet().forEach(key ->{
            List<String> values = map.get(key);
            if (values.contains(value)){
                System.out.println(key);
            }
        });
    }
}
