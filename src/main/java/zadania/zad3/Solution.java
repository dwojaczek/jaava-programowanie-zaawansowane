package zadania.zad3;

import java.util.Map;
import java.util.Set;

public class Solution {

    public void mapToString(Map<String, Integer> map){

        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        String concat = entries.stream().map(entry -> "Klucz: " + entry.getKey() + ", Wartosc: " + entry.getValue())
                .peek(el -> System.out.println(el))
                .reduce((acc, curr) -> acc + ",\n" + curr)
                .get()
                .concat(".");

        System.out.println(concat);

    }
}
