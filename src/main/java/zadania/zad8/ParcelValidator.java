package zadania.zad8;

public class ParcelValidator implements Validator{
    @Override
    public boolean validate(Parcel input) {

        if (input.getxLength() + input.getyLength() + input.getzLength() >300){
            return false;
        }
        if (input.getxLength()<30 || input.getyLength()<30 || input.getzLength()<30){
            return false;
        }
        if (input.isExpress() && input.getWeight() >15){
            return false;
        }
        if (!input.isExpress() && input.getWeight()>30){
            return false;
        }
        return true;
    }
}
