package zadania.zad38;

public class CoffeeMaker {

    public static void main(String[] args) {

        CoffeeMachine coffeeMachine = new CoffeeMachine();

        Thread t1 = new Thread(()->{
            while (true){
                coffeeMachine.makeCoffee("americano");
            }
        });
        Thread t2 = new Thread(()->{
            while (true){
                coffeeMachine.makeCoffee("espresso");
            }
        });

        Thread t3 = new Thread(()->{
            while (true){
                coffeeMachine.fillWater();
            }
        });

        t1.start();
        t2.start();
        t3.start();


    }
}
