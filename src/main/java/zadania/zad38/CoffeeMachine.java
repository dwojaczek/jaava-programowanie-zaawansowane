package zadania.zad38;

public class CoffeeMachine {

    private int waterLevel = 100;

    public synchronized void makeCoffee(String coffeeName){

        if (waterLevel ==0){
            notifyAll();
        }

        while (waterLevel ==0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Preparing coffee: "+coffeeName);
        waterLevel = waterLevel -2;
        System.out.println("water level: "+waterLevel);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void fillWater(){
        while (waterLevel >0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Filling water");
        waterLevel = 100;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        notifyAll();
    }

}
