package generics;

public class Suitcase<T extends MyInterface> {

    private T object;

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
