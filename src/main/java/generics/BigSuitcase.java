package generics;

public class BigSuitcase<T, G> {

    private T o1;
    private G o2;

    public T getO1() {
        return o1;
    }

    public void setO1(T o1) {
        this.o1 = o1;
    }

    public G getO2() {
        return o2;
    }

    public void setO2(G o2) {
        this.o2 = o2;
    }
}
