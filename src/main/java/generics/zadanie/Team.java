package generics.zadanie;

public abstract class Team implements Comparable<Team> {

    private String name;
    private int played;
    private int won;
    private int lost;
    private int tied;

    public Team(String name, int played, int won, int lost, int tied) {
        this.name = name;
        this.played = played;
        this.won = won;
        this.lost = lost;
        this.tied = tied;
    }

    public int ranking(){
        return 3*won + tied;
    }

    public int compareTo(Team other){
        if (ranking() < other.ranking()){
            return 1;
        }
        if (ranking() > other.ranking()){
            return -1;
        }
        return 0;

    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", played=" + played +
                ", won=" + won +
                ", lost=" + lost +
                ", tied=" + tied +
                '}';
    }
}
