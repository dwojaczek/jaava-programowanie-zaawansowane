package generics.zadanie;

public class Main {


    public static void main(String[] args) {

        League<FootballTeam> premierLeague = new League<>();

        FootballTeam manUtd = new FootballTeam("Man Utd", 10, 8, 1, 1);
        FootballTeam manCity = new FootballTeam("Man City", 10, 9, 0, 0);
        FootballTeam liverpool = new FootballTeam("Liverpool", 10, 10, 3, 1);
        FootballTeam spurs = new FootballTeam("Tottenham", 10, 5, 2, 3);
        FootballTeam arsenal = new FootballTeam("Arsenal", 10, 5, 3, 2);
        FootballTeam chelsea = new FootballTeam("Chelsea", 10, 7, 2, 1);

        premierLeague.addTeam(manUtd);
        premierLeague.addTeam(manCity);
        premierLeague.addTeam(liverpool);
        premierLeague.addTeam(spurs);
        premierLeague.addTeam(arsenal);
        premierLeague.addTeam(chelsea);

        premierLeague.displayTable();

    }
}
