package generics.zadanie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class League <T extends Team>{

    private List<T> teams = new ArrayList<>();

    public void addTeam(T team){
        teams.add(team);
    }

    public void displayTable(){
        Collections.sort(teams);

        teams.forEach(System.out::println);
    }
}
