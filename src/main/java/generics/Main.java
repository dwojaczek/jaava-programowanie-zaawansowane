package generics;

public class Main {

    public static void main(String[] args) {
//
//        Suitcase suitcase = new Suitcase();
//        suitcase.setObject(new Clothes("spodnie", "xl", 100));
//
//        (Clothes) suitcase.getObject()

        Suitcase<Clothes> suitcase = new Suitcase<>();
        suitcase.setObject(new Clothes("spodnie", "xl", 100));
        Clothes object = suitcase.getObject();

        Suitcase<Tool> toolSuitcase = new Suitcase<>();

        BigSuitcase<Clothes, Tool> bigSuitcase = new BigSuitcase<>();

        doSomething(suitcase);

    }

    public static void doSomething(Suitcase<?> suitcase){
        MyInterface object = suitcase.getObject();
        System.out.println(object.getName());

    }
}
