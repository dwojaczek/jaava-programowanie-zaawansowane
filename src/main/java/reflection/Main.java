package reflection;

import generics.zadanie.FootballTeam;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        MyClass myClass = new MyClass();

        FootballTeam footballTeam = new FootballTeam("sss", 123,33,44,1);

        Class<? extends MyClass> aClass = myClass.getClass();

        //pobranie wszystkich metod z klasy
        Method[] methods = aClass.getMethods();
        //wypisanie pobranych metod
        Arrays.stream(methods).forEach(m -> System.out.println(m));

        //pobranie metody doSomething
        Method doSomething = aClass.getMethod("doSomething");
        //wywolanie tej metody na obiekcie myClass
        doSomething.invoke(myClass);

        //pobranie pola 'name'
        Field name = aClass.getDeclaredField("name");
        //ustawienie mozliwosci edycji tego pola (pole private)
        name.setAccessible(true);

        //przypisanie do  pola 'name' z obiektu myClass wartosci 'JAVA'
        name.set(myClass, "JAVA");

        System.out.println(myClass.getName());

    }
}
