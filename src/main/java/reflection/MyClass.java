package reflection;

import java.lang.annotation.Target;


public class MyClass {

    private String name;
    private int someInt;

    public MyClass() {
    }

    public MyClass(String name, int someInt) {
        this.name = name;
        this.someInt = someInt;
    }

    public void doSomething(){
        System.out.println("Hello from MyClass, I am doing something very important");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSomeInt() {
        return someInt;
    }

    public void setSomeInt(int someInt) {
        this.someInt = someInt;
    }
}
