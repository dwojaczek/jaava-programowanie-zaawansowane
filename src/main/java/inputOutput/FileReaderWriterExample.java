package inputOutput;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriterExample {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter("src/main/resources/file_writer.txt");
            fileWriter.write("File writer example text");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileReader fileReader = new FileReader("src/main/resources/file_writer.txt");
            int read = fileReader.read();
            while (read !=-1){
                System.out.print((char) read);
                read = fileReader.read();
            }
          fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
