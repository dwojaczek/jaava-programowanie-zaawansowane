package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerExample {

    public static void main(String[] args) {

        File text = new File("src/main/resources/buffered_writer.txt");

        try {
            Scanner scanner = new Scanner(text);
            while (scanner.hasNext()){
                String s = scanner.nextLine();
                System.out.println(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
