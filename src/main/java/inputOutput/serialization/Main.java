package inputOutput.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

    public static void main(String[] args) {
        Animal animal = new Animal("pimpek", 7);
        try {
            animal.setAge(10);
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("animal.bin"));
            outputStream.writeObject(animal);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("animal.bin"));
            Animal readAnimal = (Animal) inputStream.readObject();
            System.out.println(readAnimal);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
