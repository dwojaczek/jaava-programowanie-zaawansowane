package inputOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Exercise {

    public static void main(String[] args) {
        File file = new File("src/main/resources/zadanie.txt");

        Map<String, Integer> wordCounter = new HashMap<>();

        int counter =0;
        try {
            Scanner scanner = new Scanner(file);

            scanner.useDelimiter(" ");
            while (scanner.hasNext()){
                String next = scanner.next();
                if(wordCounter.containsKey(next)){
                    Integer integer = wordCounter.get(next);
                    wordCounter.replace(next, integer+1);
                }
                else {
                    wordCounter.put(next, 1);
                }
                counter ++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(counter);
    }
}
