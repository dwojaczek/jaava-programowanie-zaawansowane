package inputOutput;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.List;

public class PathsExample {

    public static void main(String[] args) {

        Path path = Paths.get("src/main/resources/paths_example.txt");

        try {
            Files.writeString(path, "Example with Paths and Files classes\n second line test");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<String> strings = Files.readAllLines(path);
            System.out.println(strings);
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean exists = Files.exists(path);
        System.out.println(exists);

        try {
            FileTime lastModifiedTime = Files.getLastModifiedTime(path);
            System.out.println(lastModifiedTime);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            long size = Files.size(path);
            System.out.println(size);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
