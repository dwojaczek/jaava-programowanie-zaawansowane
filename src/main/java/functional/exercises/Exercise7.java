package functional.exercises;

import java.util.Arrays;
import java.util.List;

public class Exercise7 {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Dorota", "Andrzej", "Grazyna", "dawid", "Tomek", "brajan","leon","Zosia");

        names.stream()
                .map((String name) -> {
                    return name.toUpperCase();
                })
                .sorted()
                .filter((String name) ->{
                    return name.length()>5;
                    })
                .forEach((String name) -> {
                    System.out.println(name);
                });
    }
}
