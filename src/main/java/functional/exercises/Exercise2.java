package functional.exercises;

import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Supplier;

public class Exercise2 {

    public static String everySecondChar(String source){
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<source.length(); i++){
            if (i%2 ==1){
                sb.append(source.charAt(i));
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {

        everySecondChar("abc");


        Function<String, String> stringStringFunction = (String source) -> {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < source.length(); i++) {
                if (i % 2 == 1) {
                    sb.append(source.charAt(i));
                }
            }
            return sb.toString();
        };

      //  String apply = stringStringFunction.apply("xstdgav");
        //System.out.println(apply);

        String s = everySecond(stringStringFunction);

        Supplier<String> supplier = () -> {
            return "Hello from Supplier";
        };

        supplier.get();

    }
    public static String everySecond(Function<String, String> function){
         return  function.apply("xstdgav");
    }
}
