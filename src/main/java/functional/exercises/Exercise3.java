package functional.exercises;

import java.util.Locale;
import java.util.function.Consumer;

public class Exercise3 {

    public static void main(String[] args) {


        Consumer<String> consumer = (string) -> {
            System.out.println(string);
        };
        //consumer.accept("Hello");

        Consumer<String> consumer2 = (string) -> {
            System.out.println(string.toUpperCase(Locale.ROOT));
        };

        acceptConsumer(consumer, "hi");
        acceptConsumer(consumer2, "hi");
    }

    public static void acceptConsumer(Consumer<String> consumer, String str){
        consumer.accept(str);

    }


}


