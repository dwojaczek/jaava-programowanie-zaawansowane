package functional.exercises;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Exercise6 {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Dorota", "Andrzej", "Grazyna", "dawid", "Tomek", "brajan","leon","Zosia");

        names.stream()
                .map((String name) -> {
                    return name.toUpperCase();
                })
                .sorted()
                .forEach((String name) -> {
                    System.out.println(name);
                });
    }
}
