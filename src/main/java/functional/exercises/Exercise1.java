package functional.exercises;

public class Exercise1 {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String str  = "split this sentence";
                String[] parts = str.split(" ");

                for (String s : parts){
                    System.out.println(s);
                }
            }
        };

        Runnable lambdaRunnable = () -> {
            String str  = "split this sentence";
            String[] parts = str.split(" ");
            for (String s : parts){
                System.out.println(s);
            }
        };
    }
}
