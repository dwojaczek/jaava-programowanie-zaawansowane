package functional.exercises;

import java.util.function.Function;

public class Exercise5 {

    public static void main(String[] args) {

        Function<String, String> function = (str) -> str;
        acceptFunction(function, "Java");
    }

    public static String acceptFunction(Function<String, String> function, String str){
        String apply = function.apply(str);
        System.out.println(apply);
        return apply;
    }
}
