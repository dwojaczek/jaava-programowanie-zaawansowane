package functional.exercises;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Exercise8 {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Dorota","Ania", "Andrzej", "Grazyna", "dawid", "Tomek", "brajan", "leon", "Zosia");

        long count = names.stream()
                .map(n -> n.toUpperCase(Locale.ROOT))
                .filter(name -> name.startsWith("A"))
                .peek(el -> System.out.println(el))
                .count();

        System.out.println(count);
    }
}