package functional.exercises;

import java.util.Arrays;
import java.util.List;

public class Exercise9 {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Dorota","Ania", "Andrzej", "Grazyna", "dawid", "Tomek", "brajan", "leon", "Zosia");

        names.stream()
                .map(name -> name.substring(0,1).toUpperCase() + name.substring(1))
                .forEach(name -> System.out.println(name));
    }
}
