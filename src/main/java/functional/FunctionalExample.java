package functional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class FunctionalExample {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Filip", "Grazyna", "Grzegorz", "Kamil", "Aleksandra", "Lukasz");

       // Collections.sort(names);
       // Collections.sort(names, new ReverseOrderComparator());
//        Collections.sort(names, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o2.compareTo(o1);
//            }
//        });

//        Collections.sort(names,(String o1, String o2) ->{
//            return o2.compareTo(o1);
//        } );

//        Collections.sort(names,(o1,o2) -> {
//            return o2.compareTo(o1);
//        } );

        Collections.sort(names,(o1,o2) -> o2.compareTo(o1));

        Consumer<String> stringConsumer = (String s) -> System.out.println(s.toUpperCase(Locale.ROOT));

      //  names.stream().forEach((String name)-> System.out.println(name));

  //      names.stream().map(name -> name.toUpperCase()).forEach((String name)-> System.out.println(name));

//        String firstName = names.stream()
//                .filter(name -> name.length() > 5)
//                .findFirst().get();

//        long count = names.stream().count();
//
//        System.out.println(count);
//
//
//        List<String> collect = names.stream().map(name -> name.toUpperCase()).collect(Collectors.toList());
//        System.out.println(collect);

        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7);

        Integer sum = numbers.stream().reduce((a, c) -> a + c).get();

        Optional<Integer> max = numbers.stream().reduce((a, c) -> {
            if (a < c)
                return c;

            return a;
        });


        List<List<String>> cars = Arrays.asList(Arrays.asList("911", "Panamera"), Arrays.asList("Impreza", "Forester")
        , Arrays.asList("Q3", "A4"));

        List<String> collect = cars.stream().flatMap(car -> car.stream()).collect(Collectors.toList());


        System.out.println(cars);
        System.out.println(collect);

        // System.out.println(names);

    }
}
