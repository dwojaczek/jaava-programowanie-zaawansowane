package thread.counter;

public class Counter {

    //volatile zmusza processor do zapisywania zmiennej do wspolnej pamieci dla wszzytskich rdzeni
    //bez tego kazdy rdzen moze miec swoja wersje zmiennej
    private volatile int count;

//    public synchronized void increment(){
//        //count++;
//
//        pokazuje ze podczas inkrementacji nie mamy do czynienia z operacja atomiczną (niepodzielna)
//        count = count +1;
//
//    }

    //to samo co wyzej ale blokuje tylko wykoniane kodu wewnatrz bloku synchronized
    public void increment(){
        synchronized (this){
            count = count +1;
        }

    }

    public int getCount() {
        return count;
    }
}
