package thread.counter.producerConsumer;

import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {

        Queue<Integer> integers = new LinkedList<>();
        ProducerConsumer producerConsumer = new ProducerConsumer(integers);

        Thread t1 = new Thread(()->producerConsumer.produce());
        Thread t2 = new Thread(()-> producerConsumer.consume());

        t1.start();
        t2.start();

    }
}
