package thread.counter.producerConsumer;

import java.util.Queue;

public class ProducerConsumer {

    private Queue<Integer> integers;
    private int i =0;
    private int capacity =10;

    public ProducerConsumer(Queue<Integer> integers) {
        this.integers = integers;
    }
    public void produce(){
        while (true){
            synchronized (this) {
                while (capacity ==integers.size()){
                    try {
                        System.out.println("producer is waiting");
                        wait();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                integers.add(i++);
                System.out.println("Produced: " + i);
                notify();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    }
    public void consume(){
        while (true){

            synchronized (this) {
                while (integers.size() ==0){
                    try {
                        System.out.println("consumer is waiting");
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Integer integer = integers.poll();
                System.out.println("Consumed: " + integer);
                notifyAll();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
