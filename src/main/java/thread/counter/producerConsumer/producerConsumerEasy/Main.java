package thread.counter.producerConsumer.producerConsumerEasy;


public class Main {

    public static void main(String[] args) {

        ProduceConsumerEasy producerConsumer = new ProduceConsumerEasy();

        Thread t1 = new Thread(()->producerConsumer.produce());
        Thread t2 = new Thread(()-> producerConsumer.consume());

        t1.start();
        t2.start();
    }
}
