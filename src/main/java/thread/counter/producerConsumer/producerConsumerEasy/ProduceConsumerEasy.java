package thread.counter.producerConsumer.producerConsumerEasy;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProduceConsumerEasy {

    BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(10);
    private Integer i =0;

    public void produce(){
        while (true){
            try {
                blockingQueue.put(i++);
                System.out.println("Adding to queue: "+i);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void consume(){
        while (true){
            try {
                Integer take = blockingQueue.take();
                System.out.println("Consumed: "+take);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
