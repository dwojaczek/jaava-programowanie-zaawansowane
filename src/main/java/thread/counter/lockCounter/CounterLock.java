package thread.counter.lockCounter;

import java.util.concurrent.locks.ReentrantLock;

public class CounterLock {

    private int count;
    private ReentrantLock lock = new ReentrantLock();

    public void increment(){

        try{
            lock.lock();
            count++;
        }finally {
            lock.unlock();
        }
    }

    public int getCount() {
        return count;
    }
}
