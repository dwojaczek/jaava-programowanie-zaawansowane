package thread.counter.lockCounter;

import thread.counter.Counter;

public class Main {

    public static void main(String[] args) {

        CounterLock counter = new CounterLock();

        Thread t1 = new Thread(()->{
            for (int i=0; i<100000; i++){
                counter.increment();
            }
        });
        Thread t2 = new Thread(()->{
            for (int i=0; i<100000; i++){
                counter.increment();
            }
        });
        Thread t3 = new Thread(()->{
            for (int i=0; i<100000; i++){
                counter.increment();
            }
        });
        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(counter.getCount());

    }
}
