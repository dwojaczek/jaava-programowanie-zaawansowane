package thread;

public class ThreadExample {


    public static void main(String[] args) {

        MyThread thread = new MyThread();
        System.out.println("Hello from main thread: " + Thread.currentThread().getName());
        thread.start();
       //tak nie robic
        //thread.run();

        //drugi sposob
        Thread thread1 = new Thread(new MyThread2());
        thread1.start();

        //to samo co wyzej
        Thread thread2 = new Thread(()->{
            System.out.println("Hello from lambda thread: "+Thread.currentThread().getName());
        });
        thread2.start();



    }



}
