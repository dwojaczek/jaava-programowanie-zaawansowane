package thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadPoolExample {

    public static void main(String[] args) {

       // ExecutorService threadPool = Executors.newFixedThreadPool(3);
        ExecutorService threadPool = Executors.newCachedThreadPool();

        threadPool.execute(()-> System.out.println("Runnalble thread: "+Thread.currentThread().getName()));
        threadPool.execute(()-> System.out.println("Runnalble thread: "+Thread.currentThread().getName()));
        threadPool.execute(()-> System.out.println("Runnalble thread: "+Thread.currentThread().getName()));

        threadPool.execute(()-> System.out.println("Runnalble thread: "+Thread.currentThread().getName()));
        threadPool.execute(()-> System.out.println("Runnalble thread: "+Thread.currentThread().getName()));
        threadPool.execute(()-> System.out.println("Runnalble thread: "+Thread.currentThread().getName()));

        Future<String> submit = threadPool.submit(() -> "hello from callable ");
        try {
            System.out.println(submit.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        threadPool.shutdown();

    }
}
