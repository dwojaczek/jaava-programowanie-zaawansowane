package rekurencja;

public class Factorial {

    public static void main(String[] args) {

        System.out.println(calculateFactorialOf(4));

    }

    public static int calculateFactorialOf(int n){
        //np n= 5 -> return 5*4*3*2*1  (silnia z 0 = 1)
        if (n<=0){
            return 1;
        }
        if (n ==1){
            return 1;
        }
        return n * calculateFactorialOf(n-1);

    }
}
