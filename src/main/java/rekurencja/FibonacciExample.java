package rekurencja;

public class FibonacciExample {

    //1 1 2 3 5 8 13 21

    public static void main(String[] args) {

        System.out.println(getFiboNumber(50));
    }

    public static int getFiboNumber(long n){

        if (n<=0){
            return 0;
        }
        if (n==1){
            return 1;
        }
        if(n==2){
            return 1;
        }

        return getFiboNumber(n-2)+ getFiboNumber(n-1);

    }

}
