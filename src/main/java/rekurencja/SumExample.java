package rekurencja;

public class SumExample {

    public static void main(String[] args) {

        System.out.println(recurrentSum(2, 5));
    }

    public static int recurrentSum(int start, int stop){
//start = 2, stop 5 -> return 2+3+4+5

        if (start ==stop){
            return stop;
        }

        return start + recurrentSum(start + 1, stop);

    }
}
