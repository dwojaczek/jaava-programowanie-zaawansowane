package rekurencja;

public class Example {


    public static void main(String[] args) {

       // printNumber(5);
        printNumbersRecurrent(5);
    }

    public static void printNumber(int n){

        for (int i=n; i>=0; i--){
            System.out.println(i);
        }

    }

    public static void printNumbersRecurrent(int n){
        System.out.println(n);
        if (n==0){
            return;
        }
        printNumbersRecurrent(n-1);
    }
}
